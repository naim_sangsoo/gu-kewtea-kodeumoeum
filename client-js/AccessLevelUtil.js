/**
 * 개발 배경: 기존 Kewtea 스프링 설계에서, 권한에 대한 목록(readaccess, writeaccess)이 MySQL 의 실제 관계형을 사용하는 것이 아닌,
 * String 타입의 column 으로 정의된 JSON Array 를 사용하여야 한다는 요구가 있었음
 * 즉, 실제 권한에 대한 목록을 사용하기 편리하기 위해 String 으로 이루어진 JSON Array 를 실제 ArrayList 화 하여
 * 권한 확인 등의 메소드를 수행하는 클래스 -> 스프링에도 유사한 코드가 있으며, 목적 역시 비슷하다.
 * ++
 * ++
 * 추가적으로, 해당 코드는 내가 어떠한 권한 조건으로 읽을 수 있는지
 * (전체공개라 읽을 수 있음, 그룹공개라 읽을 수 있음, 내 컨텐츠라 읽을 수 있음)
 * 등을 표시해주는 기능이 있다.
 */

const publicId = 0;
const privateAccess = {
    accessLevel: 'private',
    accessibleAccessList: [{ type: 'private', name: 'Private' }]
};

class AccessLevelUtil {

    constructor(type) {
        this.type = type; // 'read', 'write'
    }

    //

    // 대표로 표시할 accessLevel 과 모든 읽을 수 있는 유저 목록을 return
    // (userData 가 아니라, 간략한 id/name/type)
    getAccessLevel(accessList, requesterId, groups) {
        let type = this.type;
        let rightGroupIds = [];
        let result = { accessLevel: 'private', accessibleAccessList: [] };

        try {

            // groups
            for (let i=0; i<groups.length; i++) {
                let group = groups[i];
                let groupId = group.id;
                if (-1 < accessList.indexOf(groupId)) {
                    if (group.jsonbox && group.jsonbox.accessMap) {
                        let accessMap = group.jsonbox.accessMap;
                        if (accessMap[requesterId]) {
                            let accessValue = accessMap[requesterId];
                            if (accessValue[type]) {
                                result.accessLevel = 'group';
                                result.accessibleAccessList.push({
                                    type: 'group',
                                    id: group.id,
                                    name: group.name,
                                });
                            }
                        }
                    }
                }
            }

            // public
            if (-1 < accessList.indexOf(publicId)) {
                result.accessLevel = 'public';
                result.accessibleAccessList.unshift({
                    type: 'public',
                    id: 0,
                    name: 'Public'
                });
            }

            // 위의 과정을 다 거쳤음에도, private 일 경우 이 객체는 private only 인것으로 판단합니다.
            if ('private' === result.accessLevel) {
                result = privateAccess;
            }

        } catch(reason) {
            // 예외발생 .. private 로 처리
            result = privateAccess;
        }

        return result;
    }

}

module.exports = AccessLevelUtil;
