/**
 * 스프링에서 모델링이 여러 조건 등에 의해 클라이언트에서 바로 사용하기 어려운 형태이며,
 * (유저 정보를 따로 가져와야하는 점, Array 혹은 Object 가 실제로는 JSON String 화 되어있는 점)
 * 이것을 클라이언트 측에서 사용하기 위해 변환을 해주는 코드임
 * 이때, 목적 방향에 따라
 * 서버 -> 클라이언트 parse 때는, String -> JSON Object/Array
 * 클라이언트 -> 서버 parse 때는, JSON Object/Array -> String\
 * 으로 처리한다.
 */

'use strict';
const oneHourMills = 3600000;
const currentTimezone = -new Date().getTimezoneOffset()/60;

// JSONString 값을 객체 형태로 변환 (혹은 반대로 객체를 JSON 형태로)
function DataParser(appId, logService) {

    this.parseToLocalBulk = parseToLocalBulk;
    this.parseToLocal = parseToLocal;
    this.parseToServer = parseToServer;

    function parseToLocalBulk(model, dataList) {
        let result = [];
        if (dataList) {
            for (let i=0; i<dataList.length; i++) {
                let data = dataList[i];
                result.push(parseToLocal(model, data));
            }
        }
        return result;
    }

    function parseToLocal(model, data) {
        if (data) {

            // 공통 부분 ...
            if (data.readaccess)
                data.readaccess = JSON.parse(data.readaccess);
            if (data.writeaccess)
                data.writeaccess = JSON.parse(data.writeaccess);
            if (data.jsonbox)
                data.jsonbox = JSON.parse(data.jsonbox);

            if (model === 'user') {

                if (data.memberids)
                    data.memberids = JSON.parse(data.memberids);

                try {
                    if (data.accessrights)
                        data.accessrights = JSON.parse(data.accessrights);
                } catch(reason) {
                    reason = reason.toString();
                    if (-1 < reason.indexOf('at JSON.parse') ||
                        -1 < reason.indexOf('in JSON at')) {
                        let str = data.accessrights;
                        str = str.replace('[', '').replace(']', '');
                        let array = str.split(',');
                        for (let i=0; i<array.length; i++) {
                            array[i] = array[i].trim();
                        }
                        data.accessrights = array;
                    }
                }

                return data;

            }

            if (appId === 1) {

                switch (model) {

                    case 'content' :

                        if (data.contenttype == 'chapterPage' && data['body'])
                            data.body = JSON.parse(data.body);

                        if (data.body_text)
                            data.body_text = JSON.parse(data.body_text);

                        if (data.datefrom) {
                            data.originDatefrom = data.datefrom;
                            data.datefrom = getCorrectionTimeMills(data.datefrom, data.datetimezone);
                        }
                        if (data.dateto) {
                            data.originDateto = data.dateto;
                            data.dateto = getCorrectionTimeMills(data.dateto, data.datetimezone);
                        }

                        return data;

                    case 'inventory' :

                        if (data.memberids)
                            data.memberids = JSON.parse(data.memberids);

                        if (data['setting_value'])
                            data['setting_value'] = JSON.parse(data['setting_value']);

                        return data;

                    case 'comment' :

                        data.contentids = JSON.parse(data.contentids);
                        data.collectionids = JSON.parse(data.collectionids);
                        data.keywordids = JSON.parse(data.keywordids);
                        data.ranges = JSON.parse(data.ranges);
                        data.highlights = JSON.parse(data.highlights);

                        if (data.datereference) {
                            data.originDatereference = data.datereference;
                            data.datereference =
                                getCorrectionTimeMills(data.datereference, data.createdtzone);
                        }

                        return data;

                    case 'plugin' :

                        // ...

                        return data;

                    default: logService.warn('unavailable model type', model);
                }
            }

            if (appId === 2) {

                switch (model) {

                    case 'content' :

                        if (data.contenttype === 'Block') data.contenttype = 'block';
                        if (data.contenttype === 'Scene') data.contenttype = 'scene';
                        if (data.contenttype === 'Theme') data.contenttype = 'theme';

                        // TODO
                        // jsonbox parse (map 등 없애고 쓰기 쉽게 바꾸기)
                        // ...

                        return data;


                    case 'device' :

                        // ...

                        return data;


                    case 'domain' :

                        // ...

                        return data;


                    case 'token' :


                        return data;

                    default: logService.warn('unavailable model type', model);

                }

            }


        } else {
            logService.warn('unavailable data', model, data);
        }
    }

    // 로컬 -> 서버로 전송
    function parseToServer(model, data) {
        if (data) {
            let parsedData = JSON.parse(JSON.stringify(data));

            if (data.id && -1 < data.id.toString().indexOf('local-'))
                delete parsedData.id;
            if (data.readaccess)
                parsedData.readaccess = JSON.stringify(data.readaccess);
            if (data.writeaccess)
                parsedData.writeaccess = JSON.stringify(data.writeaccess);
            if (data.jsonbox)
                parsedData.jsonbox = JSON.stringify(data.jsonbox);

            if (model === 'user') {

                if (data.memberids)
                    parsedData.memberids = JSON.stringify(data.memberids);
                if (data.accessrights && typeof data.accessrights != 'string')
                    parsedData.accessrights = JSON.stringify(data.accessrights);
                return parsedData;

            }

            if (appId === 0) {

                switch (model) {

                    case 'plugin' :

                        // ...

                        return parsedData;
                }
            }

            if (appId === 1) {

                switch (model) {

                    case 'content' :
                        if (data.body_text)
                            parsedData.body_text = JSON.stringify(data.body_text);
                        if ('chapterPage' === data.contenttype)
                            parsedData.body = JSON.stringify(data.body);

                        if (data.originDatefrom) {
                            parsedData.datefrom = data.originDatefrom;
                            delete parsedData.originDatefrom;
                        }
                        if (data.originDateto) {
                            parsedData.dateto = data.originDateto;
                            delete parsedData.originDateto;
                        }

                        return parsedData;

                    case 'inventory' :
                        if (data['setting_value'])
                            parsedData['setting_value'] = JSON.stringify(data['setting_value']);
                        return parsedData;

                    case 'comment' :
                        parsedData.contentids = JSON.stringify(data.contentids);
                        parsedData.collectionids = JSON.stringify(data.collectionids);
                        parsedData.keywordids = JSON.stringify(data.keywordids);
                        parsedData.ranges = JSON.stringify(data.ranges);
                        parsedData.highlights = JSON.stringify(data.highlights);

                        if (data.datereference) {
                            if (data.originDatereference) {
                                parsedData.datereference = data.originDatereference;
                                delete parsedData.originDatereference;
                            } else {
                                parsedData.datereference = data.datereference;
                            }
                        }

                        return parsedData;

                    default: logService.warn('unavailable model type', model);
                }

            }

            if (appId === 2) {

                switch (model) {

                    case 'content' :

                        if (data.contenttype === 'block') parsedData.contenttype = 'Block';
                        if (data.contenttype === 'scene') parsedData.contenttype = 'Scene';
                        if (data.contenttype === 'theme') parsedData.contenttype = 'Theme';

                        // TODO
                        // jsonbox map 등으로 다시 감싸기
                        // ...

                        return parsedData;

                    case 'domain' :
                    // ?
                        return parsedData;

                    case 'device' :

                        return parsedData;

                }

            }


        } else {
            logService.warn('unavailable data', model, data);
        }
    }

    function getCorrectionTimeMills(originTimeMills, originTimezone) {
        let correctionHours = currentTimezone - originTimezone;
        return originTimeMills - (correctionHours*oneHourMills);
    }

}

module.exports = DataParser;
