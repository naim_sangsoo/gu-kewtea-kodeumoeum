/**
 * 클라이언트 측에서 WebSocket 사용 연결 / 메시징 등을 하는 코드
 */

function NaimWebSocketService(initialData) {
    let serverUrl = ''; //initialData['serverUrl'];
    if (-1 < location.href.indexOf(':8080'))
        serverUrl = 'http://192.168.0.5:8080';
    else
        serverUrl = 'https://kewtea.com';

    let socketUrl = '/websocket_handler';
    let sendBasePath = '/ws_app';
    let topicPath = '/ws_topics';
    let stompClient = undefined;

    this.init = init;
    this.send = send;
    this.subscribe = subscribe;

    //

    function init() {
        return new Promise(function(resolve) {
            let socket = new SockJS(serverUrl+socketUrl);
            stompClient = Stomp.over(socket);
            stompClient['reconnect_delay'] = 5000;
            stompClient.connect({}, function() {
                console.warn('web socket onConnected');
                resolve();
            }, function(error) {
                console.warn('web socket onError', error);
            });
        });
    }

    function send(path, data) {
        console.warn('send', path, data);
        let sendData = { 'data': data };
        stompClient.send(sendBasePath+path, {}, JSON.stringify(sendData));
    }

    function subscribe(path, observer) {
        console.warn('subscribe', path);
        stompClient.subscribe(topicPath+path, function(response) {
            console.warn('ws on message', response);
            let body = JSON.parse(response['body']);
            observer(body);
        });
    }

}

module.exports = NaimWebSocketService;
