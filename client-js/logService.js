/**
 * 앵귤러에서 전역적으로 등록되어 사용 가능한 로그서비스.
 * logServiceConfig 라는 앵귤러 constant 세팅을 통해 여러 서비스로 로그 기록 가능
 * (console 창 로그, 구글 애널릭티스에 로그보고, 스프링 서버에 API 로그 보고  등 ... )
 */

angular.module('kew.logService', [])

    .constant('logServiceConfig', {
        reportServer: false,
        reportUrl: '',
        reportGA: false,
        reportGaId: '',
        reportFile: false
    })

    .service('logService', function($http, logServiceConfig) {
        let minPrintLevel = logServiceConfig.minPrintLevel || 0;
        let counterMap = {};
        let config = {
            appId: undefined,
            swVersion: undefined,
            reportServer: false,
            reportUrl: '',
            reportBatchUrl: '',
            reportGA: false,
            reportGaId: '',
            reportFile: false,
            reportSamplingRate: 1,
            reportBatchSize: 10
        };

        this.debug = debug;
        this.info = info;
        this.log = log;
        this.warn = warn;
        this.error = error;
        this.printCount = printCount;

        window.printKewLogCount = printCount;

        init();

        //

        function init() {
            setConfig();
            if (config.reportGA) setGoogleAnalytics();

            // 로그 일괄 전송을 위한 로컬 스토리지 초기화
            if (localStorage.batchedLogs == undefined) {
                localStorage.batchedLogs = JSON.stringify([]);
            }

            // 페이지 로딩 시간 전송
            sendPageLoadingTimeLog();
        }

        function setConfig() {
            if (logServiceConfig) {
                if (logServiceConfig.appId)
                    config.appId = logServiceConfig.appId;
                if (logServiceConfig.swVersion)
                    config.swVersion = logServiceConfig.swVersion;
                if (logServiceConfig.reportServer)
                    config.reportServer = logServiceConfig.reportServer;
                if (logServiceConfig.reportUrl)
                    config.reportUrl = logServiceConfig.reportUrl;
                if (logServiceConfig.reportBatchUrl)
                    config.reportBatchUrl = logServiceConfig.reportBatchUrl;
                if (logServiceConfig.reportGA)
                    config.reportGA = logServiceConfig.reportGA;
                if (logServiceConfig.reportGaId)
                    config.reportGaId = logServiceConfig.reportGaId;
                if (logServiceConfig.reportFile)
                    config.reportFile = logServiceConfig.reportFile;
                if (logServiceConfig.reportSamplingRate)
                    config.reportSamplingRate = logServiceConfig.reportSamplingRate;
                if (logServiceConfig.reportBatchSize)
                    config.reportBatchSize = logServiceConfig.reportBatchSize;
            }
            console.log('logService: config', config);
        }

        function setGoogleAnalytics() {
            let gaId = config['reportGaId'];
            if (gaId) {
                (function(i, s, o, g, r, a, m) {i['GoogleAnalyticsObject']=r; i[r]=i[r]||function() {
                    (i[r].q=i[r].q||[]).push(arguments);}, i[r].l=1*new Date(); a=s.createElement(o),
                m=s.getElementsByTagName(o)[0]; a.async=1; a.src=g; m.parentNode.insertBefore(a, m);
                })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
                ga('create', gaId, 'auto');
                ga('send', 'pageview');
            } else {
                console.error('logService: Not availale gaId: ', gaId);
            }
        }

        function debug() {
            printer('debug', arguments);
            counter(arguments);
            sendLog('dubug', arguments);
        }

        function info() {
            printer('info', arguments);
            counter(arguments);
            sendLog('info', arguments);
        }

        function log() {
            printer('log', arguments);
            counter(arguments);
            sendLog('log', arguments);
        }

        function warn() {
            printer('warn', arguments);
            counter(arguments);
            sendLog('warn', arguments);
        }

        function error() {
            printer('error', arguments);
            counter(arguments);
            sendLog('error', arguments);
        }

        //

        // TODO 호환성 문제있어서 수정함 .. (.apply 가 동작안함) (구형 chrome 및 safari)
        function printer(level, args) {
            if ('debug' === level && minPrintLevel <= 0) console.debug(args);
            if ('info' === level && minPrintLevel <= 1) console.info(args);
            if ('log' === level && minPrintLevel <= 1) console.log(args);
            if ('warn' === level && minPrintLevel <= 2) console.warn(args);
            if ('error' === level && minPrintLevel <= 3) console.error(args);
            console.trace();
        }

        function counter(args) {
            let key = args[0];
            if (counterMap[key] === undefined) {
                counterMap[key] = 0;
            }
            counterMap[key]++;
        }

        function printCount(key) {
            if (key) console.log(counterMap[key]);
            else console.log(counterMap);
        }

        /**
         * https://developer.mozilla.org/en-US/docs/Web/API/Navigation_timing_API
         */
        function sendPageLoadingTimeLog() {
            setTimeout(function() {
                sendLog('log', { 'pageLoadingTime':
                    performance.timing.loadEventEnd -
                    performance.timing.navigationStart });
            }, 10000);
        }

        function sendLog(level, args) {
            // TODO
            // ...
            // log 이상만 전송하기
            if ('log' === level || 'warn' === level || 'error' === level) {
                let msg = JSON.stringify(args);
                console.debug('logService: msg:', msg);

                if (config.reportGA && config.reportGaId) {
                    if (ga) {
                        ga('send', 'event', 'Console', level, msg);
                    } else {
                        console.warn('logService: GA service not available on this application');
                    }
                }

                // 특정 비율로 로그 기록 취소
                if (Math.random() > config.reportSamplingRate) {
                    console.debug('logService: report canceled due to the sampling rate');
                    return;
                }

                // 로그 묶음에 로그 추가
                let batchedLogs = JSON.parse(localStorage.batchedLogs);
                batchedLogs.push({
                    level: level,
                    url: location.href,
                    appid: config.appId,
                    swversion: config.swVersion,
                    msg: msg
                });

                // 충분히 로그가 쌓이지 않은 경우 로그 전송 안 하기
                if (batchedLogs.length < config.reportBatchSize) {
                    console.debug('logService: report batched');
                    console.debug('logService: batchedLogs.length:', batchedLogs.length);
                    localStorage.batchedLogs = JSON.stringify(batchedLogs);
                    return;
                }

                // 충분히 로그가 쌓인 경우 로컬 스토리지를 비우고 로그를 전송할
                // 준비하기
                let pendingLogs = batchedLogs;
                batchedLogs = [];
                localStorage.batchedLogs = JSON.stringify(batchedLogs);

                if (config.reportServer && config.reportBatchUrl) {
                    $http.post(
                        config.reportBatchUrl,
                        pendingLogs,
                        {
                            headers: {
                                'Content-Type': 'application/json',
                            }
                        }
                    ).then(function(response) {
                        console.debug('logService: log reported'+response);
                    }, function(reason) {
                        console.error(reason);
                    });
                }

                // TODO if File
                // ...
            }
        }

    });
