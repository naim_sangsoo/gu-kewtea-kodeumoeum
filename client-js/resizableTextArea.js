/**
 * textarea 사이즈가 엔터키 칠떄마다 자동으로 늘어나도록 함
 */

'use strict';
let angular = require('angular');

angular
.module('ui.resizableTextArea', [])
.directive('resizableTextarea', function() {

    return {
        template: '<textarea ng-style="{height: boxHeight+\'px\', resize: resizeAttr }"' +
                            'style="overflow-x: hidden; overflow-y: auto;"' +
                            'ng-model="data" '+
                            'ng-init="onInit()"'+
                            'ng-keydown="updateHeight($event)"' +
                            'ng-keypress="updateHeight($event)"' +
                            'ng-keyup="updateHeight($event)"></textarea>',
        scope: {
            max: '@',
            resize: '@',
            data: '=',
        },
        link: function(scope, element) {
            scope.elem = element[0].children[0];
        },
        controller: function($scope, $timeout) {

            $scope.maxHeight = Infinity;
            $scope.resizeAttr = 'none';
            $scope.updateHeight = updateHeight;
            $scope.onInit = onInit;

            if ($scope.resize) $scope.resizeAttr = $scope.resize;
            if ($scope.max) $scope.maxHeight = JSON.parse($scope.max);

            //

            function onInit() {
                $timeout(updateHeight, 10);
            }

            function updateHeight() {
                let elem = $scope.elem;
                let scrollHeight = elem.scrollHeight;
                let clientHeight = elem.clientHeight;
                if (clientHeight < scrollHeight) {
                    if (scrollHeight > $scope.maxHeight) $scope.boxHeight = $scope.maxHeight;
                    else $scope.boxHeight = scrollHeight;
                }
            }

        }
    };

});
