/**
 * 
 * 스프링 서비스로, 유틸 성격의 코드임 
 * 개발 배경: 기존 Kewtea 스프링 설계에서, 권한에 대한 목록(readaccess, writeaccess)이 MySQL 의 실제 관계형을 사용하는 것이 아닌,
 * String 타입의 column 으로 정의된 JSON Array 를 사용하여야 한다는 요구가 있었음
 * 즉, 실제 권한에 대한 목록을 사용하기 편리하기 위해 String 으로 이루어진 JSON Array 를 실제 ArrayList 화 하여
 * 권한 확인 등의 메소드를 수행하는 클래스
 */

package com.kewtea.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.kewtea.content.models.Content;
import com.kewtea.user.models.User;
import com.kewtea.user.services.KewUserService;
import com.kewtea.user.util.AccessRightUtil;

@Service
public class AccessFilterService {
	
	@Autowired
	KewUserService userService;
	
	long publicId = 0;
	
	public <T extends Content> List<T> getFilteredContentsByAuth(List<T> contents, Authentication authentication, String accessType, Class<T> classType) {
		
		List<T> filteredContents = new ArrayList<>();
		
		// get request user
		User requester = null;
		long requesterId = -1;
		List<Long> groupIds = new ArrayList<>();
		if (null != authentication) {
			requester = userService.getUserByEmail(authentication.getName());
			if (requester != null ) {
				requesterId = requester.id;
				groupIds = userService.getGroupIdsByUserId(requesterId);
			}
		}
		
		for (T content : contents) {
			String targetAccessJson = "";
			if (accessType.equals("write")) targetAccessJson = content.writeaccess;
			if (accessType.equals("read")) targetAccessJson = content.readaccess;
			
			boolean isPublic = AccessRightUtil.isAccessibleByMemberId(targetAccessJson, publicId);
			boolean isSharedWithMe = AccessRightUtil.isAccessibleByMemberId(targetAccessJson, requesterId);
			// group shared check
			boolean isGroupShared = false;
			for (long groupId : groupIds) {
				if (AccessRightUtil.isAccessibleByMemberId(targetAccessJson, groupId)) {
					isGroupShared = true;
				}
			}

			if (isPublic || isSharedWithMe || isGroupShared) {
				filteredContents.add(content);
			}
			
		}
		
		return filteredContents;
		
	}

}
