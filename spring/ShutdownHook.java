/**
 * 스프링을 종료하였을 경우를 디텍트하여 이벤트가 실행 되는 컴포넌트
 */

package com.kewtea;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.kewtea.logging.AppIds;
import com.kewtea.logging.LogWrapper;
import com.kewtea.logging.models.SysRecord;
import com.kewtea.logging.repositories.SysRecordRepository;
import com.kewtea.logging.services.LogService;
import com.kewtea.mail.services.MailService;

@Component
public class ShutdownHook implements ApplicationListener<ContextClosedEvent> {
	private LogWrapper logger;
	
	@Value("${spring.version}")
	private String swVersion;
	@Autowired
	private Environment environment;
	@Autowired
	private LogService logService;
	@Autowired
	private MailService mailService;
	@Autowired
	private SysRecordRepository sysRepo;
	
	@PostConstruct
	public void postConstruct(){
		logger = new LogWrapper(logService, this.getClass(), AppIds.COMMON, swVersion, "server");
	}
	
	@Override
	public void onApplicationEvent(ContextClosedEvent event) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd G HH:mm:ss z");
		String eventStr = event.toString();
		String currentTime = sdf.format(new Date());
		
		// sys record
		SysRecord record = new SysRecord();
		record.level="info";
		record.msg = eventStr;
		record.appid = AppIds.COMMON;
		record.swversion = swVersion;
		record.typeclass = "server-"+Long.toString(AppIds.COMMON)+"-"+swVersion;
		sysRepo.save(record);
		
		////
		
		System.out.println("Server Shutdown hooked.");
		System.out.println(eventStr);
		System.out.println("ShutdownEvent= "+eventStr);
		System.out.println(currentTime);
		
		boolean isDev = false;
		for (String profile : environment.getActiveProfiles()) {
            if (profile.equals("dev")) isDev = true;
        }
		if (!isDev) {
			try {
				mailService.send("sangsoo.lee@kewtea.com", "shutdown event hooked.", "server down at "+currentTime);			
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}			
		}
		
	}

}
