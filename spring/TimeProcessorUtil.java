/**
 * 개발 배경: 시간(Date) 값이 실제 Date 오브젝트가 아닌, long 타입의 UTC 밀리세컨드 값 (ex) 1576052497119) 으로 저장하고 있음
 * 따라서, 해당 시간 값을 읽기 쉬운 값 (XX년 XX월 XX일 등) 으로 표현하는 동작등을 수행함
 */

package com.kewtea.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;

public class TimeProcessorUtil {
	private static SimpleDateFormat dateStringFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
	private static SimpleDateFormat urlFormat = new SimpleDateFormat("yyyyMMdd");
	private static SimpleDateFormat nameFormat = new SimpleDateFormat("yyyy.MM.dd");
	private static long oneHourMills = 3600000;
	
	// A. "19700101 00:00:00" -> 0 
	public static long getTimestampByDateString(String dateStr) {
		try {
			return dateStringFormat.parse(dateStr).getTime();
		} catch (Exception exception) {
			exception.printStackTrace();
			return -1;
		}
	}
	
	// B. 0 -> "19700101"
	public static String getDateUrlByTimestamp(long timestamp) {
		try {
			Date date = new Date(timestamp);
			return urlFormat.format(date);
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}
	
	// B. 0 -> "1970.01.01"
	public static String getDateNameByTimestamp(long timestamp) {
		try {
			Date date = new Date(timestamp);
			return nameFormat.format(date);
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}
	
	// C. timestamp, timezone 을 가지고 원하는 timezone 의 timestamp return
	// ex) 한국에서 9월1일 의 timestamp 를 가지고, 미국에서의 9월1일 timestamp 를 구함
	public static long convertTimeByZone(long timestamp, long timezone, long targetTimezone) {
		int gapHours = 0;
		if (targetTimezone < timezone)
			gapHours = (int) (targetTimezone-timezone);
		else
			gapHours = (int) (timezone-targetTimezone);
		
		DateTime originTime = new DateTime(timestamp).plusHours(gapHours);
		originTime.plusHours((int)targetTimezone);
		
		return originTime.getMillis();
	}
	
	// etc
	// Date(1970.01.01 21:00:00..) -> 1970.01.01 0:0:0 의 timestamp 
	public static long getStartTimeOfDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String year = Integer.toString(cal.get(Calendar.YEAR));
		String month = Integer.toString(cal.get(Calendar.MONTH)+1);
		if (month.length() == 1) month = "0"+month;
		String day = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
		if (day.length() == 1) day = "0"+day;
		
		try {
			Date startDate = dateStringFormat.parse(year+month+day+" 00:00:00");
			return startDate.getTime();
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
	public static long getStartTimeOfDate(long date) {
		return getStartTimeOfDate(new Date(date));
	}
	
	public static long getEndTimeOfDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String year = Integer.toString(cal.get(Calendar.YEAR));
		String month = Integer.toString(cal.get(Calendar.MONTH)+1);
		if (month.length() == 1) month = "0"+month;
		String day = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
		if (day.length() == 1) day = "0"+day;
		
		try {
			Date startDate = dateStringFormat.parse(year+month+day+" 23:59:59");
			return startDate.getTime();
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
	public static long getEndTimeOfDate(long date) {
		return getEndTimeOfDate(new Date(date));
	}
	
	public static String getHumanReadableTime(long timestamp, long orginTimezone, long targetTimezone) {
		long correctionHours = targetTimezone - orginTimezone;
		timestamp = timestamp - (correctionHours * oneHourMills); 
		return getHumanReadableTime(timestamp);
	}
	
	public static String getHumanReadableTime(long timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy 'at' HH:mm");
		return sdf.format(new Date(timestamp));
	}

	public static long getCorrectionHour(long homeHour, long awayHour) {
		return awayHour - (homeHour);
	}

}
